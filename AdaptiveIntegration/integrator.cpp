#include "integrator.hh"


double integrator_reuse(function<double(double)> f,double a, double b,
 double acc, double eps, double f2, double f3, int reps){
    assert(reps < 1e6);
    double f1 = f(a+1/6.*(b-a));
    double f4 = f(a+5./6.*(b-a));
    double Q = (2.*f1+f2+f3+2.*f4)/6.*(b-a);
    double q = (f1+f2+f3+f4)/4*(b-a);
    double tol = acc + eps*fabs(Q), error = fabs(Q-q)/2;
    if (error < tol) {return Q;}
    else {
         double res1 = integrator_reuse(f,a,(a+b)/2.0,acc/sqrt(2.0),eps,f1,f2,reps+1);
         double res2 = integrator_reuse(f,(a+b)/2.0,b,acc/sqrt(2.0),eps,f3,f4,reps+1);
         return res1+res2;
    };
}

double integrator(function<double(double)> f, double a, double b, double acc, double eps){
    int reps=0;
    if(isinf(a) && isinf(b)){
        function<double(double)> f_new = [f](double x){return f(x/(1-x*x))*(1+x*x)/pow(1-x*x,2);};
        double a = -1, b = 1;
        double f2 = f_new(a+2/6.*(b-a)), f3 = f_new(a+4/6.*(b-a));
        return integrator_reuse(f_new,a,b,acc,eps,f2,f3,reps);
    } else if(isinf(a) && !isinf(b)){
        function<double(double)> f_new = [f,b](double x){return f(b-(1-x)/x)/(x*x);};
        double a = 0, b = 1;
        double f2 = f_new(a+2/6.*(b-a)), f3 = f_new(a+4/6.*(b-a));
        return integrator_reuse(f_new,a,b,acc,eps,f2,f3,reps);
    } else if(!isinf(a) && isinf(b)){
        function<double(double)> f_new = [f,a](double x){return f(a+(1-x)/x)/(x*x);};
        double a = 0, b = 1;
        double f2 = f_new(a+2/6.*(b-a)), f3 = f_new(a+4/6.*(b-a));
        return integrator_reuse(f_new,a,b,acc,eps,f2,f3,reps);
    } else{
        double f2 = f(a+2/6.*(b-a)), f3 = f(a+4/6.*(b-a));
        return integrator_reuse(f,a,b,acc,eps,f2,f3,reps);
    }
}
