#include <armadillo>
#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <functional>
#include <math.h>
#include "../ODEs/ode.hh"
using namespace std;
using namespace arma;

double integrator(function<double(double)> f, double a, double b, double acc = 1e-6, double eps = 1e-6);
double integrator_reuse(function<double(double)> f, double a, double b, double acc, double eps,
 double f2, double f3, int reps);
