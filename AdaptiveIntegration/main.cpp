#include "integrator.hh"

int n;

double f1(double x) {return sqrt(x);}
double f2(double x) {n++; return 1.0/sqrt(x);}
double f3(double x) {return log(x)/sqrt(x);}
double f4(double x) {return 4*sqrt(1-pow(1-x,2));}
double f5(double x) {return 1.0/(x*x);}
double f6(double x) {return x*x*exp(-x*x);}

vec f_ode(double t, vec& y){
    n++;
    vec dydx(1);
    dydx(0) = sqrt(t);
    return dydx;
}


int main(){

    cout << "Problem A" << endl;
    cout << "The integral of sqrt(x) from 0 to 1 is \t\t calc:"       << integrator(f1,0,1) \
        << "\t exact: " << 2.0/3.0 << endl;
    n = 0;
    cout << "The integral of 1/sqrt(x) from 0 to 1 is \t calc:"     << integrator(f2,0,1) \
        << "\t\t exact: " << 2.0 << endl;
    int n_integ = n;
    cout << "The integral of ln(x)/sqrt(x) from 0 to 1 is \t calc:" << integrator(f3,0,1) \
        << "\t exact: " << -4.0 << endl;
    cout << "The integral of 4*sqrt(1-(1-x)^2) from 0 to 1 is calc:" << integrator(f4,0,1) \
        << "\t exact: " << M_PI << endl;


    cout << endl << endl << "Problem B" << endl;
    cout << "The integral of 1/x^2 from 1 to inf is \t\t calc:" << integrator(f5,1,INFINITY) \
        << "\t\t exact: " << 1.0 << endl;
    cout << "The integral of x^2*exp(-x^2) from 1 to inf is\t calc:" << integrator(f6,-INFINITY,INFINITY) \
        << "\t exact: " << sqrt(M_PI)/2 << endl;


    n = 0;
    double a=0, b=1, h=1e-4, acc = 1e-6, eps = 1e-6;
    vec t = {a};
    vec y0={0};
    mat ys(1,1);
    ys.row(0)=y0.t();
    rkdriver(f_ode,t,ys,b,h,acc,eps);
    int n_ode = n;
    cout << endl << endl << "Problem C" << endl;
    cout << "The integral of sqrt(x) from 0 to 1, calculated with the ODE driver is: "\
        << ys(ys.n_rows-1,0)<< endl;
    cout << "This result fits with the exact result." << endl << endl;
    cout << "The result was found in "<< n_ode << " calls of the function for the ODE method and " \
        << n_integ << " with the adaptive integration method, so a factor of 4 more calls with "\
        <<"the adaptive integrator." << endl;
    return 0;
}
