#include <iostream>
#include "JacDiag.h"

//************************* Jacobian Diagonalization (Cyclic sweeps) ***************************//
int Jac_diag_cyclic(mat& A, vec& e, mat& V){
int changed, rotations=0, q, p, n=A.n_rows;
e = A.diag();
do {
    changed = 0;
    for(int p=0; p<n; p++)for(int q=p+1; q<n; q++){
        double App=e(p), Aqq=e(q), Apq=A(p,q);
        double phi = 0.5 * atan2(2*Apq,Aqq-App);
        double c=cos(phi), s=sin(phi);
        double App1 = c*c*App - 2*s*c*Apq + s*s*Aqq;
        double Aqq1 = s*s*App + 2*s*c*Apq + c*c*Aqq;
        if(App!=App1 || Aqq!=Aqq1){
            changed= 1;
            rotations++;
            e(p)   = App1;
            e(q)   = Aqq1;
            A(p,q) = 0;
            double Aip, Api, Aiq, Aqi, Vip, Viq;
		    for(int i=0;i<p;i++){
				Aip=A(i,p);
				Aiq=A(i,q);
			    A(i,p)=c*Aip-s*Aiq;
				A(i,q)=c*Aiq+s*Aip;
			}
			for(int i=p+1;i<q;i++){
				Api=A(p,i);
				Aiq=A(i,q);
				A(p,i)=c*Api-s*Aiq;
				A(i,q)=c*Aiq+s*Api;
			}
			for(int i=q+1;i<n;i++){
				Api=A(p,i);
				Aqi=A(q,i);
				A(p,i)=c*Api-s*Aqi;
				A(q,i)=c*Aqi+s*Api;
			}
			for(int i=0;i<n;i++){
				Vip=V(i,p);
				Viq=V(i,q);
				V(i,p)=c*Vip-s*Viq;
				V(i,q)=c*Viq+s*Vip;
			}
        }
    }
} while(changed!=0);
return rotations;
}


//************************ Jacobian Diagonalization (Single Eigenvalue) ************************//
int Jac_diag_single(mat& A, vec& e, mat& V){
int changed, rotations=0, q, p, n=A.n_rows;
e = A.diag();
for(p = 0; p<n; p++){
    do {
        changed=0;
        for(q=p+1;q<n;q++){
            double App=e(p), Aqq=e(q), Apq=A(p,q);
            double phi = 0.5 * atan2(2*Apq,Aqq-App);
            double c=cos(phi), s=sin(phi);
            double App1 = c*c*App - 2*s*c*Apq + s*s*Aqq;
            double Aqq1 = s*s*App + 2*s*c*Apq + c*c*Aqq;
            if(App!=App1 || Aqq!=Aqq1){
                changed= 1;
                rotations++;
                e(p)   = App1;
                e(q)   = Aqq1;
                A(p,q) = 0;
                double Aip, Api, Aiq, Aqi, Vip, Viq;
    			for(int i=0;i<p;i++){
    				Aip=A(i,p);
    				Aiq=A(i,q);
    				A(i,p)=c*Aip-s*Aiq;
    				A(i,q)=c*Aiq+s*Aip;
    			}
    			for(int i=p+1;i<q;i++){
    				Api=A(p,i);
    				Aiq=A(i,q);
    				A(p,i)=c*Api-s*Aiq;
    				A(i,q)=c*Aiq+s*Api;
    			}
    			for(int i=q+1;i<n;i++){
    				Api=A(p,i);
    				Aqi=A(q,i);
    				A(p,i)=c*Api-s*Aqi;
    				A(q,i)=c*Aqi+s*Api;
    			}
    			for(int i=0;i<n;i++){
    				Vip=V(i,p);
    				Viq=V(i,q);
    				V(i,p)=c*Vip-s*Viq;
    				V(i,q)=c*Viq+s*Vip;
    			}
            }
        }
    } while(changed!=0);
}
return rotations;
}
