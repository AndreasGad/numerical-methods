#include <math.h>
#include <fstream>
#include <armadillo>

using namespace std;
using namespace arma;

int Jac_diag_cyclic(mat& A, vec& e, mat& V);
int Jac_diag_single(mat& A, vec& e, mat& V);
