#include <iostream>
#include "JacDiag.h"
#include "../isZero.h"

int main(){
int n=5;
int rotationsA=0, rotationsB=0;

mat AA(n,n,fill::zeros);
vec eA(n);
vec eB(n);
mat VA(n,n,fill::eye);
mat VB = VA;

for(int i=0; i<n; i++)for(int j=i; j<n; j++){
    AA(i,j)=rand() %10 + 1;
    AA(j,i)=AA(i,j);
}
mat AB = AA;
mat A1 = AA;
mat DA(n,n,fill::zeros);
mat DB(n,n,fill::zeros);
rotationsA = Jac_diag_cyclic(AA,eA,VA);
DA.diag()=eA;
cout << "Problem A) " << endl;
cout << "A = " << endl <<     A1 << endl;
cout << "V^T A V = " << endl << isZero(VA.t()*A1*VA) << endl;
cout << "D = " << endl << DA << endl << endl;


rotationsB = Jac_diag_single(AB,eB,VB);
DB.diag()=eB;
cout << "Problem B) " << endl;
//cout << "A = " << endl <<     A1 << endl;
cout << "V^T A V = " << endl << isZero(VB.t()*A1*VB) << endl;
cout << "D = " << endl << DB << endl << endl;
cout << "Rotations (A,B) : " << "(" << rotationsA <<","<<rotationsB <<")"<< endl;
cout << "As we see the eigenvalues are ordered from lowest to highest. This is because of the implementation of atan2, which makes the algorithm converge towards the lowest eigenvalues first. " << endl << endl;
cout << "If we wanted to order the eigenvalues from highest to lowest,"<< endl<< "we rotate with the angle phi'=phi+pi/2. " << endl;
return 0;
}
