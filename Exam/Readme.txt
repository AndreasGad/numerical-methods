Date: 30/06-2017
Name: Andreas Gad
Student Number: 201303721
Problem Chosen: 26

Description: Multidimensional pseudo-random (plain Monte Carlo)
             vs quasi-random (Halton and/or lattice sequence) integrators


I have chosen to divide the problem into three parts, which are:

A) Implement the Halton Sequence to find quasi-random numbers.
    Show the difference between the pseudo-random and quasi-random distribution in 2D

B) Test the convergence of the plain Monte Carlo integrator vs
    the quasi-random generator for different dimensions.

C) Compare solutions for other interesting integrals.


The results from all problems are found in out.txt and there are a plot for problem A) and B)
which are found in plot_A.pdf and plot_B.pdf respectively.
