#include "montecarlo.hh"


double func_2D(vec& x){
    return x(0)*x(1);
}
double func_3D(vec& x){
    return x(0)*x(1)*x(2);
}
double func_4D(vec& x){
    return x(0)*x(1)*x(2)*x(3);
}
double func_8D(vec& x){
    return x(0)*x(1)*x(2)*x(3)*x(4)*x(5)*x(6)*x(7);
}
double gaussian(vec& x){
    return exp(-(x(0)*x(0)+x(1)*x(1)+x(2)*x(2)));
}
double fun(vec& x){
    return 1/pow((1+x(0)),2);
}
double fun2(vec& x){
    return exp(x(0));
}

int main(){
    // Problem A) Implement the Halton Sequence to find quasirandom numbers.
    //     Show the difference between the pseudo random and quasi random destribution in 2D

{
    int N_A = 300;
    vec a = {0,0};
    vec b = {1,1};
    vec x_p = {0,0};
    vec x_q = {0,0};
    ofstream fileA;
    fileA.open("data_A.dat", ios::trunc);
    for(int i=0; i<N_A; i++){
        randomgen(a,b,x_p,i);
        Halton(a,b,x_q,i);
        fileA << x_p(0) << "\t" << x_p(1) << "\t" << x_q(0) << "\t" << x_q(1) << "\n";
    }
    fileA.close();
    cout << "Problem A) \n";
    cout << "Implement the Halton Sequence to find quasirandom numbers.\n";
    cout << "Show the difference between the pseudo random and quasi random destribution in 2D\n\n";
    cout << "In plot_A.pdf, one can see that hitpatterns for the plain Monte Carlo\n";
    cout << "method and the Halton sequence in two dimensions.\n";
    cout << "In the plain Monte Carlo method, we see clustering of hits which is mostly avoided\n";
    cout << " in the Halton sequence, where the spacing is more regular.\n\n\n";
}

    // Problem B) Test the convergence of the plain Monte Carlo integrator vs
    //            the quasi randomgen for different dimensions.
    int n = 100;
    vec N_B = linspace<vec>(1,5*n,n);
    vec a1 = {0};
    vec b1 = {1};
    vec a2 = {0,0};
    vec b2 = {1,1};
    vec a3 = {0,0,0};
    vec b3 = {1,1,1};
    vec a4 = {0,0,0,0};
    vec b4 = {1,1,1,1};
    vec a8 = {0,0,0,0,0,0,0,0};
    vec b8 = {1,1,1,1,1,1,1,1};

    {
    vec res_err_quasi(2);
    vec res_err_pseudo(2);
    ofstream fileB;
    fileB.open("data_B.dat", ios::trunc);
    for(int i = 0; i<n; i++){
        res_err_pseudo = MonteCarloIntegrator(func_2D,a2,b2,N_B(i),randomgen);
        res_err_quasi  = MonteCarloIntegrator(func_2D,a2,b2,N_B(i),Halton);
        fileB << N_B(i) <<"\t"<< res_err_pseudo(0) << "\t" << res_err_quasi(0);
        fileB << "\t" << res_err_pseudo(1) << "\t" << res_err_quasi(1) << "\n" ;
    };
    fileB << "\n\n";
    for(int i = 0; i<n; i++){
        res_err_pseudo = MonteCarloIntegrator(func_3D,a3,b3,N_B(i),randomgen);
        res_err_quasi  = MonteCarloIntegrator(func_3D,a3,b3,N_B(i),Halton);
        fileB << N_B(i) <<"\t"<< res_err_pseudo(0) << "\t" << res_err_quasi(0);
        fileB << "\t" << res_err_pseudo(1) << "\t" << res_err_quasi(1) << "\n" ;
    };
    fileB << "\n\n";
    for(int i = 0; i<n; i++){
        res_err_pseudo = MonteCarloIntegrator(func_4D,a4,b4,N_B(i),randomgen);
        res_err_quasi  = MonteCarloIntegrator(func_4D,a4,b4,N_B(i),Halton);
        fileB << N_B(i) <<"\t"<< res_err_pseudo(0) << "\t" << res_err_quasi(0);
        fileB << "\t" << res_err_pseudo(1) << "\t" << res_err_quasi(1) << "\n" ;
    };
    fileB << "\n\n";
    for(int i = 0; i<n; i++){
        res_err_pseudo = MonteCarloIntegrator(func_8D,a8,b8,N_B(i),randomgen);
        res_err_quasi  = MonteCarloIntegrator(func_8D,a8,b8,N_B(i),Halton);
        fileB << N_B(i) <<"\t"<< res_err_pseudo(0) << "\t" << res_err_quasi(0);
        fileB << "\t" << res_err_pseudo(1) << "\t" << res_err_quasi(1) << "\n" ;
    };

    cout << "Problem B) \n";
    cout << "Test the convergence of the plain Monte Carlo integrator vs\n";
    cout << "the quasi randomgen for different dimensions.\n\n";
    cout << "In this part I have used the function f(x,y,z,...)=x*y*z*..., in 2, 3, 4 and 8 dimensions.\n";
    cout << "The results are shown in plot_B.pdf, where we see that the Halton sequence is more stable\n";
    cout << "than the plain Monte Carlo, and converges quickly in low dimensions. This is because the\n";
    cout << "Halton sequence converges as log(N)^dim/N, whereas the Monte Carlo converges as 1/sqrt(N).\n\n\n";
    fileB.close();
}
    // Problem C) Try other interesting integrals.

{
    int N_C = 1e4;
    vec res_err_halton_gaus3(2), res_err_pseudo_gaus3(2);
    res_err_pseudo_gaus3  = MonteCarloIntegrator(gaussian,a3,b3,N_C,randomgen);
    res_err_halton_gaus3  = MonteCarloIntegrator(gaussian,a3,b3,N_C,Halton);
    vec res_err_halton_fun(2), res_err_pseudo_fun(2);
    res_err_pseudo_fun = MonteCarloIntegrator(fun,a1,b1,N_C,randomgen);
    res_err_halton_fun = MonteCarloIntegrator(fun,a1,b1,N_C,Halton);
    vec res_err_halton_fun2(2), res_err_pseudo_fun2(2);
    res_err_pseudo_fun2 = MonteCarloIntegrator(fun2,a1,b1,N_C,randomgen);
    res_err_halton_fun2 = MonteCarloIntegrator(fun2,a1,b1,N_C,Halton);


    cout << "Problem C) \n";
    cout << "Test other functions to see how well the two methods solve the integral.\n\n";
    cout << "The results for different integrals from 0 to 1 and N=1e4 are: \n";
    cout << "Function\t\t\t Halton\t\tPlain MC\tAnalytical\n";
    cout << "f(x,y,z)=exp(-x*x-y*y-z*z)\t"<<res_err_halton_gaus3(0)<<"\t"<<res_err_pseudo_gaus3(0)<<"\t\t"<< "0.416538" <<"\n";
    cout << "f(x)=1/(1+x)^2\t\t\t"<<res_err_halton_fun(0)<<"\t"<<res_err_pseudo_fun(0)<<"\t"<<"0.5" <<"\n";
    cout << "f(x)=exp(x)\t\t\t"<<res_err_halton_fun2(0)<<"\t\t"<<res_err_pseudo_fun2(0)<<"\t\t"<<"1.7183" <<"\n";
    cout << "We see in all cases that the Halton sequence converges faster,\nwhich is expected for low dimensions.\n";
}
    return 0;
}
