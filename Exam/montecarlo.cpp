#include "montecarlo.hh"



int prime(int i){
    vec primes = { 2,    3,    5,    7,   11,   13,   17,   19,   23,   29,
       31,   37,   41,   43,   47,   53,   59,   61,   67,   71,
       73,   79,   83,   89,   97,  101,  103,  107,  109,  113,
      127,  131,  137,  139,  149,  151,  157,  163,  167,  173,
      179,  181,  191,  193,  197,  199,  211,  223,  227,  229,
      233,  239,  241,  251,  257,  263,  269,  271,  277,  281,
      283,  293,  307,  311,  313,  317,  331,  337,  347,  349,
      353,  359,  367,  373,  379,  383,  389,  397,  401,  409,
      419,  421,  431,  433,  439,  443,  449,  457,  461,  463,
      467,  479,  487,  491,  499,  503,  509,  521,  523,  541,
      547,  557,  563,  569,  571,  577,  587,  593,  599,  601,
      607,  613,  617,  619,  631,  641,  643,  647,  653,  659,
      661,  673,  677,  683,  691,  701,  709,  719,  727,  733,
      739,  743,  751,  757,  761,  769,  773,  787,  797,  809,
      811,  821,  823,  827,  829,  839,  853,  857,  859,  863};
    return primes(i);
}
void Halton(vec& a, vec& b, vec& x, int i){
    int dim = a.n_elem;
    vec t(dim,fill::zeros), prime_reci(dim,fill::zeros), result(dim,fill::zeros);
    int d;
    for(int j=0; j<dim; j++){
        t(j)=i;
        prime_reci(j) = 1.0 / (double)prime(j+1);
    }

    while( 0<accu(t) ){
        for ( int j = 0; j < dim; j++ ){
            d = (int)t(j) % prime(j+1);
            result(j) += (double) d*prime_reci(j);
            prime_reci(j) /= (double)prime(j+1);
            t(j) /= (double)prime(j+1);
        }
    }
    x = a + (b-a)%result;
}
void randomgen(vec& a, vec& b, vec& x, int j){
    assert(x.n_elem == a.n_elem && x.n_elem == b.n_elem);
    for(int i=0; i<abs(x.n_elem); i++){x[i] = a[i] + RND*(b[i]-a[i]);}
};
vec MonteCarloIntegrator(double f(vec& x), vec& a,
    vec& b, int N, void method(vec& a, vec&b, vec& x, int i)){

    assert(b.n_elem == a.n_elem);
    int dim = a.n_elem;
    vec res_err(2,fill::zeros), x(dim,fill::zeros);
    double V = 1, sum=0, sumsq=0, fx, avg, var;

    for(int i=0; i<dim; i++){V*=b[i]-a[i];}

    for(int i=0; i<N; i++){
        method(a,b,x,i);
        fx = f(x);
        sum += fx;
        sumsq += fx*fx;
    }
    avg = sum/N;
    var = sumsq/N-avg*avg;
    res_err(0) = avg*V;
    res_err(1) = sqrt(var/N)*V;
    return res_err;
};
