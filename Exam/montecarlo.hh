#include <armadillo>
#include <stdio.h>
#include <functional>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <string>
using namespace std;
using namespace arma;
#define RND ((double)rand()/RAND_MAX)

int prime(int i);
void Halton(vec& a, vec& b, vec& x, int i);
void randomgen(vec& a, vec& b, vec& x, int i);
void Corbut(vec& a, vec&b, vec& x, int i);
vec MonteCarloIntegrator(double f(vec& x), vec& a,
    vec& b, int N, void method(vec& a, vec& b, vec& x, int i));
