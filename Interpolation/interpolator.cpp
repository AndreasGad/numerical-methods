#include <iostream>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include "interpolator.hh"
using namespace std;


interpolator::interpolator(vector<double> x_vec , vector<double> y_vec, double z_val){
    setParams(x_vec, y_vec, z_val);
};

void interpolator::setParams(vector<double> x_vec, vector<double> y_vec, double z_val){
    setX(x_vec);
    setY(y_vec);
    setZ(z);
    setp();
    setc();
    setb();
};


double interpolator::getLinValue(){
    binsearch();
    LinSpline();
    return value;
};
double interpolator::getQuadValue(){
    binsearch();
    QuadSpline();
    return value;
};
double interpolator::getCubValue(){
    binsearch();
    CubSpline();
    return value;
};
double interpolator::getLintegValue(){
    binsearch();
    Linteg();
    return value;
};
double interpolator::getQuadIntegValue(){
    binsearch();
    QuadInteg();
    return value;
};
double interpolator::getQuadDiffValue(){
    binsearch();
    QuadDiff();
    return value;
};


double interpolator::getX(int i){return x[i];};
double interpolator::getY(int i){return y[i];};


// private
void interpolator::LinSpline(){
    value = y[index] + p[index] * (z-x[index]);
};

void interpolator::QuadSpline(){
    value = y[index] + b[index] * (z-x[index]) + c[index] * pow(z-x[index],2);
};

void interpolator::CubSpline(){
    value = 1;
};

void interpolator::Linteg(){
    double val=0;
    for (int i = 0; i < index; i++) {
        val+=p[i]/2.0*pow(dx[i],2)+y[i]*dx[i];
    }
    value = val + p[index]/2.0*pow(z-x[index],2)+y[index]*(z-x[index]);
};
void interpolator::QuadInteg(){
    double val=0;
    for (int i = 0; i < index; i++) {
        val+= y[i]*dx[i] + b[i]/2.0 * pow(dx[i],2) + c[i]/3.0 * pow(dx[i],3);
    }
    value = val + y[index]*(z-x[index]) + b[index]/2.0 * pow(z-x[index],2) + c[index]/3.0 * pow(z-x[index],3);
};
void interpolator::QuadDiff(){
    value = b[index] + 2*c[index] * (z-x[index]);
};



void interpolator::binsearch(){
    int n=1;
    int i=floor(x.size()/2.0);
    do {
        double delta = ceil(x.size()/pow(2,n+1));
        if(z==x[i]){break;}
        else if(z<x[i]){i -= delta;}
        else if(z>x[i]){i += delta;}
        n++;
    }while(z<=x[i] || z>=x[i+1]);
    index = i;
};

void interpolator::setX(vector<double> x_vec){
x = x_vec;
setdx();
};
void interpolator::setY(vector<double> y_vec){
y = y_vec;
setdy();
};

void interpolator::setdx(){for(int i=0; i<x.size()-1;i++){dx.push_back(x[i+1]-x[i]);}  };
void interpolator::setdy(){for(int i=0; i<y.size()-1;i++){dy.push_back(y[i+1]-y[i]);}  };
void interpolator::setp() {for(int i=0; i<x.size()-1;i++){ p.push_back(dy[i]/dx[i]);}  };
void interpolator::setb() {for(int i=0; i<x.size()-1;i++){b.push_back(p[i]-c[i]*dx[i]);}  };
void interpolator::setZ(double z_val){z = z_val;};
void interpolator::setc() {
    c.push_back(0);
    for(int i=1; i<x.size()-1;i++){
        c.push_back((p[i]-p[i-1]-c[i-1]*dx[i-1])/dx[i+1]);
    }
    c[x.size()-1]=c[x.size()-1]/2.0;
    for(int i=x.size()-2; i>0; i--){
        c[i]=(p[i+1]-p[i]-c[i+1]*dx[i+1])/dx[i];
    }
};
