#ifndef INTERPOLATOR_HH
#define INTERPOLATOR_HH
using namespace std;
class interpolator{
public:
    interpolator(vector<double> x_vec, vector<double> y_vec, double z);
    void setParams(vector<double> x_vec, vector<double> y_vec, double z);
    double getLinValue();
    double getQuadValue();
    double getCubValue();
    double getLintegValue();
    double getQuadIntegValue();
    double getQuadDiffValue();
    double getX(int i);
    double getY(int i);
    void setZ(double z_val);

private:
    void LinSpline();
    void QuadSpline();
    void CubSpline();
    void Linteg();
    void QuadInteg();
    void QuadDiff();
    void binsearch();
    void setX(vector<double> x_vec);
    void setY(vector<double> y_vec);
    void setp();
    void setdy();
    void setdx();
    void setc();
    void setb();
    double value;
    double z;
    vector<double> x;
    vector<double> y;
    vector<double> p;
    vector<double> c;
    vector<double> b;
    vector<double> dx;
    vector<double> dy;
    int index;
protected:
};
#endif
