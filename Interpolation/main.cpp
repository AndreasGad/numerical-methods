#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>
#include "interpolator.hh"
using namespace std;

int main(){

// Problem
vector<double> x, y, x_splined, y_LinSplined, y_Linteg, y_QuadSplined, y_QuadInteg;
vector<double> y_QuadDiff;
double z=0.0;
double gamma = 0.3;
ofstream file;
file.open("data.dat", ios::trunc);
for(int i=0; i<20; i++){
    x.push_back(gamma*i);
    y.push_back(cos(x[i]));
    file << x[i] << "\t" << y[i] << "\n";}
file << "\n\n";

interpolator interp(x,y,z);
for(int i=0; i<x.size()-1; i++){
    x_splined.push_back(gamma*(i+0.5));
    interp.setZ(x_splined[i]);
    y_LinSplined.push_back(interp.getLinValue());
    y_Linteg.push_back(interp.getLintegValue());
    y_QuadSplined.push_back(interp.getQuadValue());
    y_QuadInteg.push_back(interp.getQuadIntegValue());
    y_QuadDiff.push_back(interp.getQuadDiffValue());
    //y_CubSplined.push_back(interp.getCubValue());
    file << x_splined[i] << "\t" << y_LinSplined[i] <<"\t" << y_Linteg[i] << "\t"
         << "\t" << y_QuadSplined[i] << "\t" << y_QuadInteg[i]<<"\t" << -y_QuadDiff[i]<<"\n";
}

file.close();
return 0;
}
// Mangler at indsætte udtryk for c i interpolation.cpp. Derudover cubic spline.
