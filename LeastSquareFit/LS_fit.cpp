#include "LS_fit.h"

//********************************** Least Squares fit *******************************************//
void LS_fit_QR(vec& x, vec& y, vec& dy, vec& c, mat& S){
    int n = x.size(), m = c.size();
    mat A(n,m,fill::zeros);
    mat R(m,m,fill::zeros);
    mat Ri(m,m,fill::zeros);
    vec b = y/dy;
    for(int i=0; i<n; i++)for(int k=0; k<m; k++){
        A(i,k) = fit_functions(k,x(i)) / dy(i);
    }
    QR_gs_decomp(A,R);
    QR_gs_solve(A,R,b,c);
    QR_gs_inverse(R,Ri);
    S = Ri*Ri.t();
}


double fit_functions(int i, double xi){
    if     (i == 0) {return  1.0/xi;}
    else if(i == 1) {return  1;}
    else if(i == 2) {return  xi;}
    else {cerr << "fit_function number not defined."; return NAN;}
}
