#include <armadillo>
#include "../LinearEquations/QR_gs.h"

using namespace std;
using namespace arma;

void LS_fit_QR(vec& x, vec& y, vec& dy, vec& c,mat& S);
double fit_functions(int i, double xi);
