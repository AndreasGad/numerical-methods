#include "LS_fit.h"

int main(){

vec  x  = { 0.100, 0.145, 0.211, 0.307, 0.447, 0.649, 0.944, 1.372, 1.995, 2.900 };
vec  y  = {12.644, 9.235, 7.377, 6.460, 5.555, 5.896, 5.673, 6.964, 8.896, 11.355};
vec dy  = { 0.858, 0.359, 0.505, 0.403, 0.683, 0.605, 0.856, 0.351, 1.083, 1.002 };
//vec dy(x.size(),fill::ones);
//dy=dy*0.2;

vec c(3,fill::zeros);
mat S(3,3,fill::zeros);
LS_fit_QR(x,y,dy,c,S);

for(int i=0; i<abs(x.size()); i++){cout << x(i) << "\t" << y(i) << "\t" << dy(i) << endl;}
cout << endl << endl;
int size = 1000;
vec x_fit = linspace<vec>(x.min(),x.max(),size);
vec y_fit(size,fill::zeros);
vec y_upper(size,fill::zeros);
vec y_lower(size,fill::zeros);
vec deltac = sqrt(S.diag());

for(int i=0; i<size; i++){
    for(int k=0; k<3; k++){
        y_fit(i)+=c(k)*fit_functions(k,x_fit(i));
        y_upper(i) += (c(k)+deltac(k))*fit_functions(k,x_fit(i));
        y_lower(i) += (c(k)-deltac(k))*fit_functions(k,x_fit(i));
    };
cout << x_fit(i) << "\t" << y_fit(i) << "\t" << y_upper(i) << "\t" << y_lower(i) << endl;
}

return 0;
}
