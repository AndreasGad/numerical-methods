#include <iostream>
#include "QR_gs.h"




//*********************************** Decomposition ***********************************//
void QR_gs_decomp(mat& A, mat& R){
    for(int j=0; j<abs(A.n_cols); j++){
        // Calculate the norm of a_i
        double r  = dot(A.col(j),A.col(j));
        R(j,j) = sqrt(r);
        A.col(j) /= sqrt(r);
        // Orthogonalize each a_j to q_i.
        for(int jp=j+1; jp<abs(A.n_cols); jp++){
            double s = dot(A.col(j),A.col(jp));
            A.col(jp) -= s*A.col(j);
            R(j,jp) = s;
        }
    }
}


//*********************************** Solver **********************************************//
void QR_gs_solve(mat& Q, mat& R, vec& b, vec& x){
    x = Q.t()*b;
    int m = R.n_rows;
        for(int i=m-1; i>=0; i--){
            double sum = 0;
            for(int k=i+1; k<m; k++){sum += R(i,k)*x(k);}
            x(i) = (x(i) - sum )/R(i,i);
        };
};


//*********************************** Inverse **********************************************//
void QR_gs_inverse(mat& A, mat& B){
    int m = A.n_cols;
    int n = A.n_rows;
    mat R(m,m);
    QR_gs_decomp(A,R);
    mat Units(n,m,fill::eye);
    for(int i=0; i<n; i++){
        vec b = Units.col(i);
        QR_gs_solve(A,R,b,b);
        B.col(i)=b;
    };
};
