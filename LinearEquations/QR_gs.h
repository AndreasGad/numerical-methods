#include <math.h>
#include <fstream>
#include <armadillo>

using namespace std;
using namespace arma;

void QR_gs_decomp(mat& A, mat& R);
void QR_gs_solve(mat& Q, mat& R, vec& b, vec& x);
void QR_gs_inverse(mat& A, mat& B);
