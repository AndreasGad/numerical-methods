#include <iostream>
#include "QR_gs.h"
#include "../isZero.h"


int main(){

// *********************************** Problem A) *******************************************//
{int n = 4, m=3;
mat A(n,m,fill::randu);
mat Q = A;
mat R(m,m,fill::zeros);
mat B(n,m,fill::zeros);
vec b(m,fill::randu);
vec x = b;
QR_gs_decomp(Q,R);

cout << "Problem A.a) with a 4x3 matrix." << endl;
cout << "R = "      <<endl<<isZero(R)         << endl;
cout << "Q^T Q = "  <<endl<<isZero(Q.t()*Q)   << endl;
cout << "A-QR = "   <<endl<<isZero(A-Q*R)     << endl;}
//QR_gs_solve(Q,R,x,x);
//cout << "A*x-b = "   <<endl<<isZero(A*x-b)     << endl;}

// *********************************** Problem B) *******************************************//

{int n=4, m=4;
mat A(n,m,fill::randu);
mat Q = A;
mat A_i = A;
mat R(m,m,fill::zeros);
mat B(n,m,fill::zeros);
vec b(m,fill::randu);
vec x = b;

QR_gs_decomp(Q,R);
QR_gs_solve(Q,R,x,x);
QR_gs_inverse(A_i,B);

cout << "Problem A.b) and B) with a 4x4 matrix." << endl;
cout << "R = "      <<endl<<isZero(R)         << endl;
cout << "A-QR = "   <<endl<<isZero(A-Q*R)     << endl;
cout << "A*x-b = "  <<endl<<isZero(A*x-b)     << endl;
cout << "A*A^-1 = " <<endl<<isZero(A*B)       << endl;}

return 0;
}
