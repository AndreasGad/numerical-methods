#include "montecarlo.hh"


double f(vec& x){
    return pow(1-cos(x(0))*cos(x(1))*cos(x(2)),-1)/pow(M_PI,3);
}
double test(vec& x){
    return sqrt(x(0));
}

int main(){
    int N_a = 1e7;
    //vec err_a(n,fill::zeros);
    vec a = {0,0,0};
    vec b = {M_PI,M_PI,M_PI};
    vec res_err_a(2);
    res_err_a = plainmc(f,a,b,N_a);

    int n = 100;
    vec N = logspace<vec>(1,7,n);
    //vec err(n,fill::zeros);
    //vec a = {0,0,0};
    //vec b = {M_PI,M_PI,M_PI};
    vec res_err(2);
    ofstream file;
    file.open("data.dat", ios::trunc);
    for(int i = 0; i<n; i++){
        res_err = plainmc(test,a,b,N(i));
        file <<N(i) <<"\t"<< res_err(1)<< "\n" ;
    };
    file.close();
    cout << "The result is: " << res_err_a(0) << "("<<res_err_a(1)<<")"<<endl;
    cout << "The N dependency of error is seen in plot.pdf.\n Here it is tested on the function sqrt(x), and is seen to fit nicely to 1/sqrt(x)." << endl;
    return 0;
}
