#include "montecarlo.hh"




void randomgen(vec& a, vec& b, vec& x){
    assert(x.n_elem == a.n_elem && x.n_elem == b.n_elem);
    for(int i=0; i<abs(x.n_elem); i++){x[i] = a[i] + RND*(b[i]-a[i]);}
};


vec plainmc(double f(vec& x), vec& a, vec& b, int N){
    assert(b.n_elem == a.n_elem);
    int dim = a.n_elem;
    vec res_err(2,fill::zeros), x(dim,fill::zeros);
    double V = 1, sum=0, sumsq=0, fx, avg, var;
    for(int i=0; i<dim; i++){V*=b[i]-a[i];}
    for(int i=0; i<N; i++){
        randomgen(a,b,x);
        fx = f(x);
        sum += fx;
        sumsq += fx*fx;
    }
    avg = sum/N;
    var = sumsq/N-avg*avg;
    res_err(0) = avg*V;
    res_err(1) = sqrt(var/N)*V;
    return res_err;
};
