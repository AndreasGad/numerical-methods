#include <armadillo>
#include <stdio.h>
#include <functional>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
using namespace std;
using namespace arma;
#define RND ((double)rand()/RAND_MAX)

void randomgen(vec& a, vec& b, vec& x);
vec plainmc(double f(vec& x), vec& a, vec& b, int N);
