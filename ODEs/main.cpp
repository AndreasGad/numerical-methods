#include "ode.hh"
#include "../isZero.h"
#include <fstream>


vec f(double t, vec& y){
    vec dydx(2);
    dydx(0) = y(1);
    dydx(1) = -y(0);
    return dydx;
}


int main(){
    double a=0, b=4, h=1e-4, acc=1e-6, eps=1e-6;
    vec t = {a};
    vec y0={0,1};
    mat ys(1,2);
    ys.row(0)=y0.t();
    rkdriver(f,t,ys,b,h,acc,eps);
    ofstream file;
    file.open("data.dat", ios::trunc);
    for(int i=0; i<abs(ys.n_rows); i++){
        file << t(i) << "\t" << ys(i,0) << "\t"<< ys(i,1) << "\n" ;
    }
    file.close();


    return 0;
}
