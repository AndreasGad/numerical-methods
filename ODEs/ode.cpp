#include "ode.hh"


int rkstep12(vec f(double t, vec& y), double t, vec& y, double h, vec& yh, vec& err){
    vec k0 = f(t,y);
    double tp = t+h;
    vec yp =y+h*k0;
    vec k1 = f(tp,yp);
    yh  = y+0.5*h*(k0+k1);
    err = 0.5*h*(k1 - k0);
    return 1;
};

void rkdriver(vec f(double t, vec& y), vec& t, mat& ys, double b, double h, double acc, double eps){
    int n = t.n_elem;
    int index = 0;
    vec y(n), dy(n), y0(n);
    double t0, err, tol;
    double a = t(0);
    while(t(t.n_elem-1) < b){
        index = t.n_elem;
        t0 = t(index-1);
        y0 = ys.row(index-1).t();
        if(b<t0+h){h=b-t0;}
        rkstep12(f,t0,y0,h,y,dy);
        err = norm(dy);
        tol = (acc+norm(y)*eps)*sqrt(h/(b-a));
        if(err<tol){
            t.resize(index+1);
            t(index) = t0+h;
            ys.insert_rows(index,y.t());
        }
	    if(err>0){h = h*pow(tol/err,0.25)*0.95;}
		else     {h = 2*h;}
    }
};
