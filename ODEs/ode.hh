#include <armadillo>
#include<functional>
#include "../LinearEquations/QR_gs.h"
using namespace std;
using namespace arma;


int rkstep12(double t, double h, vec& y, function<vec(double,vec)> & f, vec& yh, vec& err);
void rkdriver(vec f(double t, vec& y), vec& t, mat& y_stored, double t_end, double h, double acc,
    double eps);
