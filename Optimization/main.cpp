#include "minimizer.h"
#include "../isZero.h"
#include <fstream>


vec t = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
vec y = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
vec e = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
int N = t.size();

// Rosenbrock
double f_rosen(vec& input){
    double x = input(0), y = input(1);
    double fx = (1-x)*(1-x)+100*(y-x*x)*(y-x*x);
    return fx;
}
vec gradient_rosen(vec& input){
    double x = input(0), y = input(1);
    vec dfdx(2);
    dfdx(0) = 400*x*x*x + 2*x - 2 - 400*x*y;
    dfdx(1) = 200*(y-x*x);
    return dfdx;
}
mat hessian_rosen(vec& input){
    double x = input(0), y = input(1);
    mat ddfddx(2,2);
    ddfddx(0,0) = 1200*x*x + 2 - 400*y;
    ddfddx(0,1) = -400*x;
    ddfddx(1,0) = -400*x;
    ddfddx(1,1) = 200;
    return ddfddx;
}

// Himmelblau
double f_himmel(vec& input){
    double x = input(0), y = input(1);
    double fx = (x*x+y-11)*(x*x+y-11) + (x+y*y-7)*(x+y*y-7);
    return fx;
}
vec gradient_himmel(vec& input){
    double x = input(0), y = input(1);
    vec dfdx(2);
    dfdx(0) = 2*2*x*(x*x+y-11)+2*(x+y*y-7);
    dfdx(1) = 2*(x*x+y-11) + 2*2*y*(x+y*y-7);
    return dfdx;
}
mat hessian_himmel(vec& input){
    double x = input(0), y = input(1);
    mat ddfddx(2,2);
    ddfddx(0,0) = 2*(x*x+y-11) + 2*x*2*x + 1;
    ddfddx(0,1) = 2*x + 2*y;
    ddfddx(1,0) = 2*x + 2*y;
    ddfddx(1,1) = 1 + 2*(x+y*y-7) + 2*y*2*y;
    return ddfddx;
}


// Exponential Decay
double exponential(vec& input, double t){
    double A = input(0), T = input(1), B = input(2);
    double fx = A*exp(-t/T)+B;
    return fx;
}
double data_minimization(vec& input){
    double fx = 0;
    for(int i=0; i<N; i++){fx += pow(exponential(input,t(i))-y(i),2)/as_scalar(e(i)*e(i));}
    return fx;
}


int main(){
    cout << "******************  PROBLEM A/B ***********************" << endl << endl;
    // ***************************  Rosenbrock function ***************************************** //
    vec x = {4,5};
	cout << "Minimum of Rosenbrock function:" << endl;
	cout << "Initial vector is " << endl << "x0= " << endl << x << endl;
	cout << "f(x0)= "  << f_rosen(x) << endl;
    int ncalls_rosen_classic = minimizer_newton(f_rosen,gradient_rosen,hessian_rosen,x,1e-6);
    cout << endl <<"Solution is" << endl << "x= " << endl << x << endl;
	cout << "f(x)= " << isZero(f_rosen(x)) << endl;
    cout << "The minimum was found in " << ncalls_rosen_classic << " iterations." << endl;
    // ***************************  Himmelblau function ***************************************** //
        x = {4,5};
    cout << endl <<"Minimum of Himmelblau function:" << endl;
    cout << "Initial vector is " << endl << "x0= " << endl << x << endl;
    cout << "f(x0)= "  << f_himmel(x) << endl;
    int ncalls_himmel_classic = minimizer_newton(f_himmel,gradient_himmel,hessian_himmel,x,1e-6);
    cout << endl <<"Solution is" << endl << "x= " << endl << x << endl;
    cout << "f(x)= " << isZero(f_himmel(x)) << endl;
    cout << "The minimum was found in " << ncalls_himmel_classic << " iterations." << endl;

    // ***************************  Broyden with Gradient *************************************** //
        x = {4,5};
    int ncalls_rosen_quasi_with = minimizer_broyden(f_rosen,gradient_rosen,x,1e-6);

        x = {4,5};
    int ncalls_himmel_quasi_with = minimizer_broyden(f_himmel,gradient_himmel,x,1e-6);
    // ***************************  Broyden w/o Graident **************************************** //
        x = {4,5};
    int ncalls_rosen_quasi_wo = minimizer_broyden(f_rosen,x,1e-6);

        x = {4,5};
    int ncalls_himmel_quasi_wo = minimizer_broyden(f_himmel,x,1e-6);



    cout << endl << endl;
    cout << "The number of iterations for the classic Newtons method";
    cout << ", Broydens with Gradient, Broydens without gradient and from the Rootfinding problem is:" << endl;
    cout << "Rosenbrock: \t" << ncalls_rosen_classic << ", \t" << ncalls_rosen_quasi_with;
    cout << ", \t" << ncalls_rosen_quasi_wo<<", \t" << 355 << endl;
    cout << "Himmelblau: \t" << ncalls_himmel_classic << ", \t" << ncalls_himmel_quasi_with;
    cout << ", \t" << ncalls_himmel_quasi_wo<<", \t" << 7 << endl;

    cout << endl << endl << "******************  PROBLEM B ***********************" << endl << endl;
        vec data_x = {1,1,1};
    int data_calls = minimizer_broyden(data_minimization,data_x,1e-6);
    cout << "The best fit parameters for the exponential decay was found in "<<data_calls;
    cout << " iterations, and are: " << endl << "A = " << data_x(0) << endl;
    cout << "T = " << data_x(1) << endl;
    cout << "B = " << data_x(2) << endl;
    cout << "The plot of the fit can be seen in output.pdf" << endl << endl;

    ofstream file;
    file.open("data.dat", ios::trunc);
    for(int i=0;i<N;i++){file << t(i)<<"\t"<<y(i)<<"\t"<<e(i)<<"\n";}
    file << "\n\n";
    for(int i=0;i<1000;i++){
        double tid = t(0) + (t(N-1)-t(0))*i/1000.0;
        file << tid <<"\t"<<exponential(data_x,tid)<<"\n";
    }
    file.close();
    return 0;
}
