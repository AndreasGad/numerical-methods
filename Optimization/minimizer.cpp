#include "minimizer.h"


vec gradient(double f(vec& x), vec& x, double dx){
    int n = x.size();
    vec dfdx(n);
    double fx = f(x);
    for(int i=0; i<n; i++){
        x(i) += dx;
        dfdx(i) = (f(x)-fx)/dx;
        x(i) -= dx;
    };
    return dfdx;
}

int minimizer_newton(double f(vec& x), vec gradient(vec& x), mat hessian(vec& x), vec& x, double eps){
    int n = x.size();
    int steps = 0;
    double lambda_min = 0.02;
    double alpha = 1e-4;
    double fx, fxp;
    vec grad(n), Dx(n), xp(n);
    mat R(n,n), hess(n,n), hess_inv(n,n);
    do {
        steps++;
        fx = f(x);
        grad = gradient(x);
        hess = hessian(x);
        QR_gs_decomp(hess,R);
        QR_gs_solve(hess,R,grad,Dx);

        double lambda = 2;
        double dxdf = as_scalar(Dx.t()*grad);
        while(1){
            lambda/=2;
    	    xp=x-Dx*lambda;
    	    fxp=f(xp);
            if(fxp<= (fx + alpha*lambda*dxdf) || lambda<lambda_min ){break;}
        }
        x=xp; fx=fxp;
    } while(fx>eps);
    return steps;
}

int minimizer_broyden(double f(vec& x), vec gradient(vec& x), vec& x, double eps){
    int n = x.size();
    int steps = 0;
    double fx=f(x), lambda, alpha = 1e-4, dx = 1e-6;
    vec s(n), y(n), Dx(n), xp(n);
    mat Hi(n,n,fill::eye);
    vec dfdx = gradient(x), dfdxp(n);
    do {
        steps++;
        Dx = - Hi * dfdx;
        lambda = 2;
        do {
            lambda /= 2;
            s = Dx*lambda;
            xp = s+x;
            if(norm(s)<dx){Hi.eye();};
        } while(abs(f(xp)) > abs(fx) + alpha*lambda*dot(Dx.t(),dfdx) && norm(s)>dx);

        dfdxp = gradient(xp);
        y = dfdxp-dfdx;
        Hi += (s-Hi*y)*(s.t()*Hi)/dot(y.t(), Hi*s);
        x = xp;
        fx = f(x);
        dfdx = dfdxp;
    }  while(norm(dfdx)>eps);
    return steps;
}

int minimizer_broyden(double f(vec& x), vec& x, double eps){
    int n = x.size();
    int steps = 0;
    double fx=f(x), lambda, alpha = 1e-4, dx = 1e-6;
    vec s(n), y(n), Dx(n), xp(n);
    mat Hi(n,n,fill::eye);
    vec dfdx = gradient(f,x,dx), dfdxp(n);
    do {
        steps++;
        Dx = - Hi * dfdx;
        lambda = 2;
        do {
            lambda /= 2;
            s = Dx*lambda;
            xp = s+x;
            if(norm(s)<dx){Hi.eye();};
        } while(abs(f(xp)) > abs(fx) + alpha*lambda*dot(Dx.t(),dfdx) && norm(s)>dx);

        dfdxp = gradient(f,xp,dx);
        y = dfdxp-dfdx;
        Hi += (s-Hi*y)*(s.t()*Hi)/dot(y.t(), Hi*s);
        x = xp;
        fx = f(x);
        dfdx = dfdxp;
    }  while(norm(dfdx)>eps);
    return steps;
}
