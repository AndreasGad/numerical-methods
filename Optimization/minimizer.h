#include <armadillo>
#include "../LinearEquations/QR_gs.h"
using namespace std;
using namespace arma;

vec calc_gradient(double f(vec& x), vec& x, double dx);

int minimizer_newton(double f(vec& x), vec gradient(vec& x), mat hessian(vec&x), vec& x, double eps);

int minimizer_broyden(double f(vec& x), vec gradient(vec& x), vec& x, double eps);

int minimizer_broyden(double f(vec& x), vec& x, double eps);
