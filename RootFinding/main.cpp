#include "rootfinder.h"
#include "../isZero.h"

//int ncalls;

vec f1(vec& input){
    //ncalls++;
    double x = input(0), y = input(1);
    double A = 10000;
    vec fx(2);
    fx(0) = exp(-x) + exp(-y) - (1/A + 1);
    fx(1) = A*x*y - 1;
    return fx;
}
vec f_rosen(vec& input){
    //ncalls++;
    double x = input(0), y = input(1);
    vec fx(2);
    fx(0) = -2*(1-x)-100*2*(y-x*x)*2*x;
    fx(1) = 100*2*(y-x*x);
    return fx;
}
vec f_himmel(vec& input){
    //ncalls++;
    double x = input(0), y = input(1);
    vec fx(2);
    fx(0) = 2*(x*x+y-11) + 2*2*y*(x+y*y-7);
    fx(1) = 2*2*x*(x*x+y-11)+2*(x+y*y-7);
    return fx;
}
mat jacobian_rosen(vec& input){
    double x = input(0), y = input(1);
    mat J(2,2,fill::zeros);
    J(0,0) =  1200*x*x + 2 - 400*y;
    J(0,1) = -400*x;
    J(1,0) = -400*x;
    J(1,1) =  200;
    return J;
}
mat jacobian_himmel(vec& input){
    double x = input(0), y = input(1);
    mat J(2,2);
    J(0,0) = 2*(x*x+y-11) + 2*x*2*x + 1;
    J(0,1) = 2*x + 2*y;
    J(1,0) = 2*x + 2*y;
    J(1,1) = 1 + 2*(x+y*y-7) + 2*y*2*y;
    return J;
}

int main(){

    // ***************************  Eq. System function ******************************************//
    vec x = {4,5};
    cout << "Root of the Equation system gradient:" << endl;
    cout << "initial vector is " << endl << "x0= " << endl << x << endl;
    cout << "f(x0)= " << endl << f1(x) << endl;
    int steps_1 = rootfinder(f1,x,9e-8,1e-6);
    cout << "Solution is" << endl << "x= " << endl << x << endl;
    cout << "f(x)= " << endl << isZero(f1(x)) << endl;
    cout << "The solution was found in " << steps_1 << " iterations" << endl;


    // ***************************  Rosenbrock function ******************************************//
        x = {4,5};
//	cout << "Root of the Rosenbrock's function gradient:" << endl;
//	cout << "initial vector is " << endl << "x0= " << endl << x << endl;
//	cout << "f(x0)= " << endl << f_rosen(x) << endl;
	int steps_rosen_1 = rootfinder(f_rosen,x,9e-8,1e-6);
//    cout << "Solution is" << endl << "x= " << endl << x << endl;
//	cout << "f(x)= " << endl << isZero(f_rosen(x)) << endl;
//    cout << "The solution was found in " << steps_rosen_1 << " iterations" << endl;


    // ***************************  HimmelBlau function ******************************************//
        x = {4,5};
//    cout << "Root of the HimmelBlau's function gradient:" << endl;
//    cout << "initial vector is " << endl << "x0= " << endl << x << endl;;
//    cout << "f(x0)= " << endl << f_himmel(x) << endl;
    int steps_himmel_1 = rootfinder(f_himmel,x,9e-8,1e-6);
//    cout << "Solution is" << endl << "x= " << endl << x << endl;
//    cout << "f(x)= " << endl << isZero(f_himmel(x)) << endl;
//    cout << "The solution was found in " << steps_himmel_1 << " iterations" << endl;

    // ***************************  Rosenbrock function ******************************************//
        x = {4,5};
	cout << "Root of the Rosenbrock's function gradient:" << endl;
	cout << "initial vector is " << endl << "x0= " << endl << x << endl;
	cout << "f(x0)= " << endl << f_rosen(x) << endl;
    int steps_rosen_2 = rootfinder_with_jacobian(f_rosen,jacobian_rosen,x,1e-6);
	cout << "Solution is" << endl << "x= " << endl << x << endl;
	cout << "f(x)= " << endl << isZero(f_rosen(x)) << endl;
    //cout << "The solution was found in " << steps_rosen_2 << " iterations vs "<< steps_rosen_1 << " when not using the analytical Jacobian." << endl;

        x = {4,5};
    cout << "Root of the Himmelblau's function gradient:" << endl;
    cout << "initial vector is " << endl << "x0= " << endl << x << endl;
    cout << "f(x0)= " << endl << f_rosen(x) << endl;
    int steps_himmel_2 = rootfinder_with_jacobian(f_rosen,jacobian_rosen,x,1e-6);
    cout << "Solution is" << endl << "x= " << endl << x << endl;
    cout << "f(x)= " << endl << isZero(f_rosen(x)) << endl;
    //cout << "The solution was found in " << steps_himmel_2 << " iterations vs "<< steps_himmel_1 << " when not using the analytical Jacobian." << endl;

    cout << endl << "The solutions were found in the following iterations with";
    cout << " and without the analytical jacbian, respectively"<< endl;
    cout << "Rosenbrock : \t" << steps_rosen_2 << ", \t" << steps_rosen_1 << endl;
    cout << "Himmelblau : \t" << steps_himmel_2 << ", \t" << steps_himmel_1 << endl;

    return 0;
}
