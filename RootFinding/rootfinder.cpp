#include "rootfinder.h"


int rootfinder(vec f(vec& x), vec& x, double dx, double epsilon){
    int n = x.size();
    int steps = 0;
    double step_min = 0.02;
    mat J(n,n);
    mat R(n,n);
    vec fx(n), Dx(n), fxp(n), xp(n);
    while(1){
        fx = f(x);
        for(int j=0;j<n;j++){
            x(j)+=dx;
            vec df = f(x)-fx;
    		for(int i=0;i<n;i++){J(i,j) = df(i)/dx;}
            x(j)-=dx;
		}             //  Here the Jacobian is created.
        QR_gs_decomp(J,R);
        QR_gs_solve(J,R,fx,Dx);
        if(norm(Dx)<dx) {break;}
        double step = 2;
        while(1){
            step/=2;
    	    xp=x-Dx*step;
    	    fxp=f(xp);
            if(norm(fxp)<(1-step/2)*norm(fx) || step<step_min ){break;}
        }
        x=xp; fx=fxp;
        steps++;
        if(norm(fx)<=epsilon){break;};
    }
    return steps;
};

int rootfinder_with_jacobian(vec f(vec& x), mat jacobian(vec& x),vec& x, double epsilon){
    int n = x.size();
    int steps = 0;
    double step_min = 0.02;
    mat R(n,n);
    vec fx(n), Dx(n), fxp(n), xp(n);
    while(1){
        fx = f(x);
        mat J = jacobian(x);
        QR_gs_decomp(J,R);
        QR_gs_solve(J,R,fx,Dx);
        double step = 2;
        while(1){
            step/=2;
            xp=x-Dx*step;
            fxp=f(xp);
            if(norm(fxp)<(1-step/2)*norm(fx) || step<step_min ){break;}
        }
        x=xp; fx=fxp;
        if(norm(fx)<=epsilon){break;}
        steps++;
    }
    return steps;
};
