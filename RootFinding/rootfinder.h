#include <armadillo>
#include "../LinearEquations/QR_gs.h"
using namespace std;
using namespace arma;


int rootfinder(vec f(vec& x), vec& x0, double dx, double epsilon);
int rootfinder_with_jacobian(vec f(vec& x), mat jacobian(vec& x),vec& x, double epsilon);
