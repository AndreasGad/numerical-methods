#include <math.h>
#include <fstream>
#include <armadillo>

using namespace std;
using namespace arma;
//*******************************  Extra functions *****************************************//
mat isZero(mat A){
    mat B(A.n_rows,A.n_cols);
    for(int i=0; i<abs(A.n_rows); i++){
        for(int j=0; j<abs(A.n_cols); j++){
            if(abs(A(i,j))<=1e-7){B(i,j)=0.0;}
            else{B(i,j)=A(i,j);}
        };
    };
    return B;
}

double isZero(double A){
    double B;
    if(abs(A)<=1e-7){B=0.0;}
    else{B=A;}
    return B;
}
