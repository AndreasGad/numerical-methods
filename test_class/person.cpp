#include <iostream>
#include "person.hh"
using namespace std;

person::person(string nm){
    setName(nm);
    sayHi();
}
person::~person(){          // This is the destructor
    cout << "Goodbye " << getName() << endl;
}

void person::sayHi(){               // First public function
    cout << "Hello " << getName() << endl;
}
void person::setName(string nm){
    name = nm;
}
string person::getName(){
    return name;
}
