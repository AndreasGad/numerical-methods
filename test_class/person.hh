#ifndef PERSON_HH
#define PERSON_HH

#include <iostream>
using namespace std;
class person{
public:
    person(string nm);
    ~person();
    void setName(string nm);
    string getName();
    void sayHi();
private:
    string name;
};
#endif
